# Vision

Our economies are at a crossroads.  We are rapidly entering into a third wave of capitalism that will accelerate economic growth in emerging markets and developing nations.  Crypto currencies and blockchain
technology represent the foundation for this evolution. Open source technology, as represented by this project, will allow governments to embrace crypto capitalism rather than attempting to handicap it. This
project approaches the regulatory issue by providing a regulatory sandbox for crypto payment systems as well the resulting "fog computing" ecosystem that will emerge from blockchain technology. 

# Opportunity

This project proposes to deploy blockchain infrastructure in a regulatorily compliant manner within jurisdictions 

# Crypto Capitalism

# Opportunity

# Establishing a Regulatory Infrastructure


