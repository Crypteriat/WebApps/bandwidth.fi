# Vision

Our economies are at a crossroads.  We are rapidly entering into a third wave of capitalism that will accelerate economic growth in emerging markets and developing nations.  Crypto currencies and blockchain
technology represent the foundation for this evolution. Open source technology, as represented by this project, will allow governments to embrace crypto capitalism rather than attempting to handicap it. This
project approaches the regulatory issue by providing a regulatory sandbox for crypto payment systems as well the resulting "fog computing" ecosystem that will emerge from blockchain technology. 

# Crypto Capitalism

# Opportunity

This project proposes to deploy blockchain infrastructure in a regulatorily compliant manner within jurisdictions while preserving user privacy and access.  We are beginning this initiative with the website,
[www.ShaktiCrypto.org](https://www.ShaktiCrypto.org). We are establishing a crypto donation system along with the necessary facilities to process payments, including [Lightning Bitcoin](https://lightning.network).
The development of this infrastructure will establish a ground floor for more advanced systems to support crypto capitalism.  We envision enhancing this infrastructure to enable businesses operating
within a given jurisdiction to offer broker, dealer, exchange, and crypto offering services. The infrastructure will support a "hardened" security approach so that operators, and licensees, may participate
in a larger system that complies with crypto friendly regulations and better secures the overall network against attacks and financial loss.

# Establishing a Regulatory Infrastructure

# Conclusions and Next Steps

